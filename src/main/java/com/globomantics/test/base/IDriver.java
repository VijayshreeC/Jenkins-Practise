package com.globomantics.test.base;

import org.openqa.selenium.WebDriver;

public interface IDriver {
    public WebDriver getDriver(Browser browser);
}
